#include <HCSR04.h>
#include <WiFi.h>


/*
 * definitions
 */
#define SERIAL_RATE 9600
#define GPIO_LEN 48

// pin assignment
#define GPIO_LED 32
#define GPIO_ULTRASONIC_TRIG 34
#define GPIO_ULTRASONIC_ECHO 35

/*
 * constants
 */
// wifi configuration
// TODO: Add wifi configuration
const char* ssid = "";
const char* password = "";

// distance normalization
const int normCycles = 4;
const double normThreshold = 0.05;
/*
 * static
 */
static boolean ioSig[GPIO_LEN] = {};
static UltraSonicDistanceSensor distSensor = UltraSonicDistanceSensor(GPIO_ULTRASONIC_TRIG, GPIO_ULTRASONIC_ECHO);

/*
 * GPIO helper functions
 */
void initGpio() {
  for (int i = 0; i < GPIO_LEN; i++) {
    set(i, LOW);
  }
}

void set(byte i, uint8_t sig) {
  digitalWrite(i, sig);
  ioSig[i] = sig == HIGH;
  Serial.print("Set GPIO ");
  Serial.print(i);
  Serial.print(" to ");
  Serial.println(ioSig[i]);
}

bool get(byte i) {
  return ioSig[i];
}

bool toggle(byte i) {
  //ioSig[i] = !ioSig[i];
  set(i, !ioSig[i]);
  return get(i);
}


/*
 * wifi
 */

void scanNetworks(bool print) {
  Serial.println("Scanning WIFI networks...");
  int noNetworks = WiFi.scanNetworks();

  if (!print)
    return;

  for (int i = 0; i < noNetworks; i++) {
    Serial.print("Network name: ");
    Serial.println(WiFi.SSID(i));
    
    Serial.print("Signal strength: ");
    Serial.println(WiFi.RSSI(i));

    Serial.print("MAC address: ");
    Serial.println(WiFi.BSSIDstr(i));
  }
}

void connectWifi(const char* ssid, const char* password) {
  Serial.print("Connecting to");
  Serial.print(ssid);

  WiFi.begin(ssid, password);

  bool toggle = true;

  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");

    if (toggle) {
      digitalWrite(GPIO_LED, HIGH);
    } else {
      digitalWrite(GPIO_LED, LOW);
    }

    toggle = !toggle;
    delay(500);
  }

  Serial.println("Connection established.");
  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());
}


/*
 * sensors
 */
double getRawDistance() {
  return distSensor.measureDistanceCm();
}

double getNormalizedDistance() {
  double dist = 0.0;

  // TODO: Include threshold;
  
  for (int i = 0; i < normCycles; i++) {
    dist += getRawDistance();
  }

  return dist / normCycles;
}


/*
 * setup
 */
void setup() {
  // initialize digital pin LED_BUILTIN as an output
  //pinMode(LED_BUILTIN, OUTPUT);

  Serial.begin(SERIAL_RATE);
  Serial.println("");
  Serial.println("Setup: Begin");

  // GPIOs
  pinMode(GPIO_LED, OUTPUT);
  pinMode(GPIO_ULTRASONIC_TRIG, OUTPUT);
  pinMode(GPIO_ULTRASONIC_ECHO, INPUT);

  initGpio();

  // wifi
  scanNetworks(false);
  connectWifi(ssid, password);

  Serial.print("IP: ");
  Serial.println(WiFi.localIP());

  Serial.println("Wifi configured.");
  WiFi.disconnect(true);

  digitalWrite(GPIO_LED, LOW);
  Serial.println("Setup: End");
}

/*
 * main loop
 */
void loop() {
  /*
  digitalWrite(LED_GPIO, HIGH);
  delay(1000);

  digitalWrite(LED_GPIO, LOW);
  delay(1000);
  */

  delay(1000);
  toggle(GPIO_LED);
  double dist;
  dist = getRawDistance();
  Serial.print("Raw distance: ");
  Serial.println(dist);
  
  dist = getNormalizedDistance();
  Serial.print("Normalized distance: ");
  Serial.println(dist);

  delay(1000);
  toggle(GPIO_LED);
}
